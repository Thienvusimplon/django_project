from django.db import models

# Create your models here.
class Members(models.Model):
  firstname = models.CharField(max_length=255)
  lastname = models.CharField(max_length=255)

class Hotel(models.Model):
    name = models.CharField(max_length=50)
    image_to_upload = models.ImageField(upload_to='images/')