from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from .views import SignUpView

urlpatterns = [
    path('', views.accueil, name='acceuil'),
    path('add/', views.add, name='add'),
    path('add/addrecord/', views.addrecord, name='addrecord'),
    path('delete/<int:id>', views.delete, name='delete'),
    path('update/<int:id>', views.update, name='update'),
    path('update/updaterecord/<int:id>', views.updaterecord, name='updaterecord'),
    path('home/', views.home, name='home'),
    path('image_upload', views.hotel_upload, name = 'image_upload'),
    path('ia', views.ia, name = 'ia'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('signin/', views.signin, name="signin"),   
]

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)