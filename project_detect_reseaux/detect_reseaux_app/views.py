from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from .models import Members
from .forms import *
import torch
import os
from PIL import Image
import pandas as pd
import json
from ia.parseq.strhub.data.module import SceneTextDataModule
import numpy as np
import shutil
import cv2
from io import BytesIO
import base64
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.decorators import login_required
from .forms import SignUpForm
from django.contrib.auth import login, authenticate


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"

def home(request):
    return HttpResponse("Hello World")

@login_required
def hotel_upload(request):
    if request.method == 'POST':
        form = HotelForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('acceuil')
    else:
        form = HotelForm()
    return render(request, 'image_upload.html', {'form' : form})

def to_image(numpy_img):
    img = cv2.resize(numpy_img, (270,220))
    img = Image.fromarray(img, 'RGB')
    return img

def to_data_uri(pil_img):
    data = BytesIO()
    pil_img.save(data, "JPEG") # pick your format
    data64 = base64.b64encode(data.getvalue())
    return u'data:img/jpeg;base64,'+data64.decode('utf-8')

@login_required
def ia(request):
    if request.GET.get('Traitement ia') == 'Traitement ia':
        model = torch.hub.load('ia/yolov5', 'custom', source='local', path = 'ia/yolov5/best.pt', force_reload = True)
        model.conf = 0.60
        parseq = torch.hub.load('baudm/parseq', 'parseq', pretrained=True).eval()
        img_transform = SceneTextDataModule.get_transform(parseq.hparams.img_size)

        isJsonExist = os.path.exists("media/json")
        if not isJsonExist:  
        # Create a new directory if does not exist 
            os.makedirs("media/json")

        entries = os.listdir('media/images/')   
        for entry in entries:
            image_file = f"media/images/{entry}"
            image_name = entry

            # Recherche des centres d'intérêt + detection de couleur
            predictions = model(Image.open(image_file))

            # Preparation du csv pour download
            df = pd.DataFrame(predictions.pandas().xyxy[0])
            df = df.rename(columns={"confidence": "confiance_couleur", "name": "couleur"})
            df = df.drop([df.columns[5]], axis=1)
            df["x_center"] = (df["xmax"] + df["xmin"]) / 2
            df["y_center"] = (df["ymax"] + df["ymin"]) / 2

            index = len(predictions.crop()) - 1
            crop_number = 1

            df_image = pd.DataFrame()
            for i in range(len(predictions.crop())):     
                label_confidence = predictions.crop()[i]["label"].split() 
                afficher_crop_img = cv2.cvtColor(predictions.crop()[i]["im"], cv2.COLOR_BGR2RGB)

                pil_image = to_image(afficher_crop_img)
                image_uri = to_data_uri(pil_image)
                detect_number_img = Image.fromarray(predictions.crop()[i]["im"])
                detect_number_img_2 = img_transform(detect_number_img).unsqueeze(0)
                logits = parseq(detect_number_img_2)
                pred = logits.softmax(-1)
                label, confidence = parseq.tokenizer.decode(pred)
                df.at[index, "numero"] = label[0]
                df.at[index, "confidence_numero"] = np.mean(confidence[0].detach().numpy())
                df_image.at[index, "numero"] = f"Numéro détecté : {label[0]}"
                df_image.at[index, "confidence_numero"] = f" Score de confiance numéro : {round((float(np.mean(confidence[0].detach().numpy()))*100),2)}%"
                df_image.at[index, "image"] = image_uri
                df_image.at[index, "label"] = f"Couleur détecté : {label_confidence[0]}"
                df_image.at[index, "score"] = f"Score de confiance couleur : {round((float(label_confidence[1])*100),2)}%"
                index -= 1
                crop_number += 1

            df = df[["xmin", "xmax", "ymin", "ymax", "x_center", "y_center", "couleur", "confiance_couleur", "numero", "confidence_numero"]]
            df.to_json(f"media/json/{image_name}.json", orient="columns")

        json_records = df.reset_index().to_json(orient ='records')
        image_records = df_image.reset_index().to_json(orient ='records')
        data = []
        data = json.loads(json_records)
        image = []
        image = json.loads(image_records)
        context = {'data': data, 'image':image}
        shutil.rmtree('runs/')
        return render(request, 'dataframe.html', context)
    else:
        context = { 
            'mymembers': 'test'
        }
        return render(request, 'test.html', context)

def accueil(request):
    mymembers = Members.objects.all().values()
    template = loader.get_template('index.html')
    context = {
    'mymembers': mymembers,
    }
    return render(request, 'index.html', context)
  
def add(request):
    template = loader.get_template('add.html')
    return HttpResponse(template.render({}, request))

def addrecord(request):
    x = request.POST['first']
    y = request.POST['last']
    member = Members(firstname=x, lastname=y)
    member.save()
    return HttpResponseRedirect(reverse('index'))

def delete(request, id):
    member = Members.objects.get(id=id)
    member.delete()
    return HttpResponseRedirect(reverse('index'))

def update(request, id):
    mymember = Members.objects.get(id=id)
    template = loader.get_template('update.html')
    context = {
    'mymember': mymember,
    }
    return HttpResponse(template.render(context, request))

def updaterecord(request, id):
    first = request.POST['first']
    last = request.POST['last']
    member = Members.objects.get(id=id)
    member.firstname = first
    member.lastname = last
    member.save()
    return HttpResponseRedirect(reverse('acceuil'))

def signin(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('acceuil')
    else:
        form = SignUpForm()
    return render(request, 'signin.html', {'form': form})