from django.apps import AppConfig


class DetectReseauxAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'detect_reseaux_app'
